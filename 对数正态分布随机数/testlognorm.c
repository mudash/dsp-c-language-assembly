//产生50个参数为u 为0， sigma为0.5的对数正太分布的随机数
#include <stdio.h>
#include "../lib/inc/signal_generator.h"

int main(void)
{
	int i,j;
	long int *s;
	double x, u, sigma;
	u = 0.0;
	sigma = 0.5;
	s = 13579;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 5; j++)
		{
			x = lognorm(u, sigma, &s);
			printf(" %13.7f", x);
		}
		printf("\n");
	}

	getchar();

	return 0;
}