
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
*/

int main()
{
	int i, n;
	double x[8];
	n = 8;
	for (i = 0; i < n; i++) {
		x[i] = exp(-0.5 * i);
	}
	printf("\n Input Sequence\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fct8(x, n);
	printf("\n Discrete Cosine Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	ifct8(x, n);
	printf("\n Inverse Discrete Cosine Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	getchar();

	return 0;
}