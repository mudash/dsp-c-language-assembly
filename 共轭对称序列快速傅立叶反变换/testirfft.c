
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
*/

int main()
{
	int i, n;
	double x[32];
	n = 32;
	for (i = 0; i < n; i++)
	{
		x[i] = exp(-i / 15.0) * sin(6.2831853 * i / 16.0);
	}
	printf("\n Original Sequence\n");
	for (i = 0; i < n; i += 4)
	{
		printf(" %10.7f  %10.7f", x[i], x[i + 1]);
		printf(" %10.7f  %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	rfft(x, n);
	printf("\n Discrete Fourier Transform\n");
	printf(" %10.7f               ", x[0]);
	printf(" %10.7f + J %10.7f\n", x[1], x[n - 1]);
	for (i = 2; i < (n / 2); i += 2)
	{
		printf(" %10.7f + J %10.7f", x[i], x[i + 1]);
		printf(" %10.7f + J %10.7f", x[i + 1], x[i + 3]);
		printf("\n");
	}
	printf(" %10.7f                ", x[n / 2]);
	printf(" %10.7f + J %10.7f\n", x[n / 2 - 1], - x[n / 2 + 1]);
	for (i = 2; i < n / 2; i += 2)
	{
		printf(" %10.7f + J %10.7f", x[n / 2 - i], -x[n / 2 + i]);
		printf(" %10.7f + J %10.7f", x[n / 2 - i - 1], -x[n / 2 + i + 1]);
		printf("\n");
	}
	irfft(x, n);
	printf("\n Inverse Discrete Fourier Transform %d \n", n);
	for (i = 0; i < n; i += 4)
	{
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	getchar();
}