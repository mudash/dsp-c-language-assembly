/**
 * 产生50个参数n = 5, p = 0.7 的二项式分布的随机数
 */
#include <stdio.h>
#include "../lib/inc/signal_generator.h"

int main()
{
	int i,j,n;
	int x;
	long int s;
	double p;
	n = 5;
	p = 0.7;
	s = 13579;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 5; j++)
		{
			x = bin(n, p, &s);
			printf("%11d", x); 
		}
		printf("\n");
	}
	getchar();
}