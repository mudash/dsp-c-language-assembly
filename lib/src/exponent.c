/**
 * 产生指数分布的随机数
 */
#include "math.h"
#include "../inc/signal_generator.h"
/**
 * beta : 		指数分布的均值
 * s ：			*s为随机数种子
 * return ：	返回随机数
 */
double exponent(double beta, long int *s)
{
	double u, x;
	u = uniform(0.0, 1.0, s);
	x = -beta * log(u);
	return (x);
}