/**
 * 快速余弦反变换
 */
#include <stdlib.h>
#include <math.h>
#include "../inc/signal_process.h"
/**
 * x ：		长度为n的double数组，开始存放变换输入数据,
 *			最后存放变换完成数据
 * n ：		int数据长度必须是2的整数次幂
 */
void ifct(double *x, int n)
{
	int i, n1;
	double q, c, *y;
	y = malloc(n * sizeof(double));
	n1 = n / 2;
	q = 4.0 * atan(1.0) / (2 * n);
	x[0] /= sqrt(2.0);
	for (i = 0; i < n; i++) {
		y[i] = x[i] * sin(i * q);
		x[i] = x[i] * cos(i * q);
	}

	fft(x, y, n, -1);
	for (i = 0; i < n; i++) {
		y[i] = n * x[i];
	}
	c = sqrt(2.0 / n);
	for (i = 0; i < n1; i++) {
		x[2 * i] = c * y[i];
		x[2 * i + 1] = c*y[n - 1 - i];
	}
	free(y);
}