/**
 * 产生对数正太分布的随机数
 */
#include <math.h>
#include "../inc/signal_generator.h"
/**
 * u : 		对数正太分布的参数
 * sigma ：	对数正太分布的参数
 * s ：		*s为随机数种子
 * return 	随机数
 */
double lognorm(double u, double sigma, long int *s)
{
	double x, y;

	y = gauss(u, sigma, s);
	x = exp(y);

	return (x);
}