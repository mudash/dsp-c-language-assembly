/**
 * shirpz变换算法
 */
#include <math.h>
#include <stdlib.h>
#include "../inc/signal_process.h"
/**
 * xr ：		长度为n的double数组，[in]存放要变换数据的实部,
 * xi ：		长度为n的double数组，[in]存放要变换数据的虚部,
 * n ：		int数据, 输入数据长度
 * m ：		整型变量，输出数据长度，即频率采样点数
 * f1 :		双精度实型变量。起始数字频率。
 * f2 :		双精度实型变量，终止数字频率。
 */
void czt(double *xr, double *xi, int n, int m, double f1, double f2)
{
	int i, j, n1, n2, len;
	double e, t, ar, ai, ph, pi, tr, ti, *wr, *wr1, *wi, *wi1;

	len = n + m - 1;
	for (j = 1, i = 1; i < 16; i++)
	{
		j = 2 * j;
		if (j >= len)
		{
			len = j;
			break;
		}
	}
	wr = malloc(len * sizeof(double));
	wi = malloc(len * sizeof(double));
	wr1 = malloc(len * sizeof(double));
	wi1 = malloc(len * sizeof(double));
	pi = 3.14159265358979;
	ph = 2.0 * pi * (f2 - f1) / (m - 1);
	n1 = (n >= m) ? n : m;
	for (i = 0; i < n1; i++)
	{
		e = ph * i * i / 2.0;
		wr[i] = cos(e);
		wi[i] = sin(e);
		wr1[i] = wr[i];
		wi1[i] = -wi[i];
	}
	n2 = len - n + 1;

	for (i = m; i < n2; i++)
	{
		wr[i] = 0.0;
		wi[i] = 0.0;
	}

	for (i = n2; i < len; i++)
	{
		j = len - i;
		wr[i] = wr[j];
		wi[i] = wi[j];
	}
	fft(wr, wi, len, 1);
	ph = -2.0 * pi * f1;
	for (i = 0; i < n; i++)
	{
		e = ph * i;
		ar = cos(e);
		ai = sin(e);
		tr = ar * wr1[i] - ai * wi1[i];
		ti = ai * wr1[i] + ar * wi1[i];
		t = xr[i] * tr - xi[i] * ti;
		xi[i] = xr[i] * ti + xi[i] * tr;
		xr[i] = t;
	}

	for (i = n; i < len; i++)
	{
		xr[i] = 0.0;
		xi[i] = 0.0;
	}

	fft(xr, xi, len, 1);
	for (i = 0; i < len; i++)
	{
		tr = xr[i] * wr[i] - xi[i] * wi[i];
		xi[i] = xr[i] * wi[i] + xi[i] * wr[i];
		xr[i] = tr;
	}
	fft(xr, xi, len, -1);
	for (i = 0; i < m; i++)
	{
		tr = xr[i] * wr1[i] - xi[i] * wi1[i];
		xi[i] = xr[i] * wi1[i] + xi[i] * wr1[i];
		xr[i] = tr;
	}
	free(wr);

	free(wi);
	free(wr1);
	free(wi1);
}