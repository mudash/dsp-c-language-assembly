/**
 * 产生含有高斯白噪声的正弦组合信息
 */
#include <math.h>
#include "../inc/signal_generator.h"
/**
 * a ：		长度为m的double数组，保存各个正弦信号的振幅
 * f ：		长度为m的double数组，保存各个正弦信号的频率
 * ph ：		长度为m的double数组，保存各个正弦信号的相位
 * m ：		int正弦信号个数
 * fs : 	double采样频率
 * snr ：	double信噪比
 * seed ：	*s为随机数种子
 * x ：		double数组，长度为n，存放所产生的数据。
 * n ：		数据的长度
 */
void sinwn(double *a, double *f, double *ph, int m, double fs, double snr, long seed, double *x, int n)
{
	int i, k;
	double z,pi,nsr;
	pi = 4.0 * atan(1.0);
	z = snr / 10.0;
	z = pow(10.0, z);
	z = 1.0 / (2 * z);
	nsr = sqrt(z);
	for (i = 0; i < m; i++)
	{
		f[i] = 2 * pi * f[i] / fs;
		ph[i] = ph[i] * pi / 180.0;
	}
	for (k = 0; k < n; k++)
	{
		x[k] = 0.0;
		for (i = 0; i < m; i++)
		{
			x[k] = x[k] + a[i] * sin(k * f[i] + ph[i]);
		}
		x[k] = x[k] + nsr * gauss(0.0, 1.0, &seed);
	}
}