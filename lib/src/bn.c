/**
 * 产生贝努里分布的随机数
 */
#include <math.h>
#include "../inc/signal_generator.h"
/**
 * p :		贝努里分布的参数p
 * s ：		*s为随机数种子
 * return ：	产生的随机数
 */
int bn(double p, long int *s) 
{
	int x;
	double u;

	u = uniform(0.0, 1.0, s);

	x = (u <= p) ? 1 : 0;

	return (x);
}