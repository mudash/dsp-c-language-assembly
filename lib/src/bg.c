/**
 * 产生贝努里-高斯分布的随机数
 */
#include <math.h>
#include "../inc/signal_generator.h"
/**
 * p :		贝努里分布的参数p
 * mean :	高斯分布的均值
 * sigma ：	高斯分布的方差
 * s ：		*s为随机数种子
 * return ：	产生的随机数
 */
double bg(double p, double mean, double sigma, long int *s)
{
	double x, u;

	u = uniform(0.0, 1.0, s);

	if (u <= p)
	{
		x = gauss(mean, sigma, s);
	}
	else
	{
		x = 0.0;
	}

	return (x);
}