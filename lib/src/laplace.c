/**
 * 产生拉普拉斯分布的随机数
 */
#include <math.h>
#include "../inc/signal_generator.h"
/**
 * beta : 		拉普拉斯分布参数
 * s ：			*s 为随机数种子
 */
double laplace(double beta, long int *s)
{
	double u1, u2, x;
	
	u1 = uniform(0.0, 1.0, s);
	u2 = uniform(0.0, 1.0, s);
	if (u1 <= 0.5)
	{
		x = -beta * log (1.0 - u2);
	}
	else
	{
		x = beta * log(u2);
	}
	return (x);
}