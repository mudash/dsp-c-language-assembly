
/**
 * 产生柯西分布的随机数
 */
#include <math.h>
#include "../inc/signal_generator.h"
/**
 * a :		柯西分布的均值
 * b ：		柯西分布的方差
 * s ：		*s为随机数种子
 * return ：	产生的随机数
 */
double cauchy(double a, double b, long int *s) 
{
	double u, x;

	u = uniform(0.0, 1.0, s);
	x = a - b / tan(3.1415926 * u);

	return (x);
}