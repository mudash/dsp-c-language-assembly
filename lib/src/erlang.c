/**
 * 产生爱尔朗分布的随机数
 */
#include <math.h>
#include "../inc/signal_generator.h"
/**
 * m :		爱尔朗分布的参数m
 * beta ：	爱尔朗分布的参数bate
 * s ：		*s为随机数种子
 * return ：	产生的随机数
 */
double erlang(int m, double beta, long int *s) 
{
	int i;
	double u, x;
	for (u = 1.0, i = 0; i < m; i++)
	{
		u *= uniform(0.0, 1.0, s);
	}
	x = -beta * log(u);

	return (x);
}