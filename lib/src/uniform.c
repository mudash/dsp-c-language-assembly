/**
 * 产生（a，b）区间上平均分布的随机数
 * a ： 	给定区域的下限
 * b ： 	给定区域的上限
 * seed：	*seed为随机数种子
 */
double uniform(double a, double b, long int *seed)
{
	double t;
	*seed = 2045 * (*seed) + 1;
	*seed = *seed - (*seed / 1048576) * 1048576;
	t = (*seed) / 1048576.0;
	t = a + (b - a) * t;
	return (t);
}