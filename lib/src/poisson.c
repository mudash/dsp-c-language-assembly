/**
 * 产生泊松分布的随机数
 */
#include "math.h"
#include "../inc/signal_generator.h"
/**
 * lambda : 	泊松分布的均值
 * s ：			*s为随机数种子
 * return ：	返回随机数
 */
int poisson(double lambda, long int *s)
{
	int i, x;
	double a, b, u;
	a = exp(-lambda);
	i = 0;
	b = 1.0;
	do
	{
		u = uniform(0.0, 1.0, s);
		b *= u;
		i++;
	} while (b >= a);
	x = i - 1;

	return (x);
}