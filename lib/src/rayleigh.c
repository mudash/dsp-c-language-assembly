/*
 * 产生瑞利分布的随机数
 */
#include <math.h>
#include "../inc/signal_generator.h"
/**
 * sigma : 		瑞利分布的参数
 * s ： 		*s 为随机数种子
 */
double rayleigh(double sigma, long int *s)
{
	double u, x;

	u = uniform(0.0, 1.0, s);
	x = -2.0 * log(u);
	x = sigma * sqrt(x);
	return (x);
}