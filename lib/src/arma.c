/**
 * 产生自回归滑动平均模型的随机数
 */
#include <stdlib.h>
#include "../inc/signal_generator.h"
/**
 *
 * a ：		长度为（p + 1）的自回归系数
 * b ：		长度为（q + 1）的滑动平均系数
 * p ：		自回归阶数
 * q ：		滑动平均阶数
 * mean : 	产生白噪声所用的高斯布的均值
 * sigma ：	产生白噪声所用的高斯布的均方差
 * seed ：	*s为随机数种子
 * x ：		ARMA模型处理的数据，长度为n
 * n ：		ARMA模型处理的数据的长度
 */
void arma(double *a, double *b, int p, int q, double mean, double sigma, long int *seed, double *x, int n)
{
	int i, k, m;
	double s, *w;
	w = malloc(n * sizeof(double));
	for (k = 0; k < n; k++)
	{
		w[k] = gauss(mean, sigma, seed);
	}
	x[0] = b[0] * w[0];
	for (k = 1; k <= p; k++)
	{
		s = 0.0;
		for (i = 1; i <= k; i++)
		{
			s += a[i] * x[k - i];
		}
		s = b[0] * w[k] - s;
		if (q == 0)
		{
			x[k] = s;
			continue;
		}
		m = (k > q) ? q : k;
		for (i = 1; i <= m; i++)
		{
			s += b[i] * w[k - i];
		}
		x[k] = s;
	}
	for (k = (p + 1); k < n; k++)
	{
		s = 0;
		for (i = 1; i <= p; i++)
		{
			s += a[i] * x[k - i];
		}
		s = b[0] * w[k] - s;
		if (q == 0)
		{
			x[k] = s;
			continue;
		}
		for (i = 1; i <= q; i++)
		{
			s += b[i] * w[k - i];
		}
		x[k] = s;
	}
	free(w);
}