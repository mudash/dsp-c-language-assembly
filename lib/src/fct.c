/**
 * 快速离散余弦变换
 */
#include <stdlib.h>
#include <math.h>
#include "../inc/signal_process.h"
/**
 * x ：		长度为n的double数组，开始存放变换输入数据,
 *			最后存放变换完成数据
 * n ：		int数据长度必须是2的整数次幂
 */
void fct(double *x, int n)
{
	int i, n1;
	double q, c, s, *y;
	y = malloc(n * sizeof(double));
	n1 = n / 2;
	for (i = 0; i < n1; i++) {
		y[i] = x[2 * i];
		y[n - 1 - i] = x[2 * i + 1];
	}
	rfft(y, n);
	q = 4.0 * atan(1.0) / (2 * n);
	x[0] = y[0];
	x[n1] = sin(n1 * q) * y[n1];
	for (i = 1; i < n1; i++) {
		c = cos(i * q);
		s = sin(i * q);
		x[i] = c * y[i] + s * y[n - i];
		x[n - i] = s * y[i] - c * y[n - i];
	}

	c = 1.0 / sqrt((double)n);
	x[0] = c * x[0];
	c = sqrt(2.0 / n);
	for (i = 1; i < n; i++) {
		x[i] = c * x[i];
	}
	free(y);
}