/**
 * 快速哈特莱(Hartley)变换算法
 */
#include <math.h>
#include <stdlib.h>
#include "../inc/signal_process.h"
/**
 * x ：		长度为n的double数组，[in]存放要变换数据的实数据，最后保存变换结果,
 * n ：		int数据, 输入数据长度,必须是2的m次方， m为正数
 */
void fht(int n, double *x)
{
	int i, j, k, m, l1, l2, l3, l4, n1, n2, n4;
	double a, e, c, s, t, t1, t2;
	for (j = 1,i = 1; i < 16; i++) {
		m = i;
		j = 2 * j;
		if (j == n) {
			break;
		}
	}
	n1 = n - 1;
	for (j = 0, i = 0; i < n1; i++) {
		if (i < j) {
			t = x[j];
			x[j] = x[i];
			x[i] = t;
		}
		k = n / 2;
		while (k < (j + 1)) {
			j = j - k;
			k = k / 2;
		}
		j = j + k;
	}
	for (i = 0; i < n; i += 2) {
		t = x[i];
		x[i] = t + x[i + 1];
		x[i + 1] = t - x[i + 1];
	}
	n2 = 1;
	for (k = 2; k <= m; k++) {
		n4 = n2;
		n2 = n4 + n4;
		n1 = n2 + n2;
		e = 6.283185307179586 / n1;
		for (j = 0; j < n; j += n1) {
			l2 = j + n2;
			l3 = j + n4;
			l4 = l2 + n4;
			t = x[j];
			x[j] = t + x[l2];
			x[l2] = t - x[l2];
			t = x[l3];
			x[l3] = t + x[l4];
			x[l4] = t - x[l4];
			a = e;
			for (i = 1; i < n4; i++) {
				l1 = j + i;
				l2 = j - i + n2;
				l3 = l1 + n2;
				l4 = l2 + n2;
				c = cos(a);
				s = sin(a);
				t1 = x[l3] * c + x[l4] * s;
				t2 = x[l3] * s - x[l4] * c;
				a = (i + 1) * e;
				t = x[l1];
				x[l1] = t + t1;
				x[l3] = t - t1;
				t = x[l2];
				x[l2] = t + t2;
				x[l4] = t - t2;
			}
		}
	}
}