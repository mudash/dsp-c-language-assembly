/**
 * N=8的快速离散余弦变换
 */
#include <stdlib.h>
#include <math.h>
#include "../inc/signal_process.h"
/**
 * x ：		长度为n的double数组，开始存放变换输入数据,
 *			最后存放变换完成数据
 * n ：		int数据长度必须是2的整数次幂
 */
void fct8(double *x, int n)
{
	int i, j, k, n1, n2;
	double t, pi, coef[8];
	n1 = n / 2;
	n2 = n / 4;
	pi = 3.141592654;
	t = x[2];
	x[2] = x[3];
	x[3] = t;
	t = x[4];
	x[4] = x[7];
	x[7] = x[5];
	x[5] = x[6];
	x[6] = t;
	for (i = 0; i < n1; i++) {
		t = x[i] + x[n1 + i];
		x[i + n1] = x[i] - x[n1 + i];
		x[i] = t;
	}
	for (i = 0; i < n2; i++) {
		coef[i] = 1 / (2 * cos((2 * i + 1) * pi / (2 * n)));
		coef[i + n1] = 1 / (2 * cos((2 * i + 1) * 2 * pi / (2 * n)));
		coef[i + n2] = 1 / (2 * cos((n - (2 * i + 1))* pi / (2 * n)));
	}
	for (i = 0; i < n2; i++) {
		x[i + n1] = x[i + n1] * coef[i];
		x[i + n1 + n2] = x[i + n1 + n2] * coef[i + n2];
	}
	for (i = 0; i < n2; i++) {
		for (j = 0; j < n2; j++) {
			t = x[4 * i + j] + x[4 * i + j + n2];
			x[4 * i + j + n2] = x[4 * i + j] - x[4 * i + j + n2];
			x[4 * i + j] = t;
		}
	}
	for (i = 0; i < n2; i++) {
		for (j = 0; j < n2; j++) {
			x[4 * i + j + n2] = x[4 * i + j + n2] * coef[j + n1];
		}
	}
	for (i = 0; i < n1; i++) {
		t = x[2 * i] + x[2 * i + 1];
		x[2 * i + 1] = x[2 * i] - x[2 * i + 1];
		x[2 * i] = t;
	}
	t = 1 / (2 * cos((4 * pi / (2 * n))));
	for (i = 1; i < n; i += 2) {
		x[i] = x[i] * t;
	}
	for (i = 0; i < n2; i++) {
		x[4 * i + n2] = x[4 * i + n2] + x[4 * i - 1 + n1];
	}
	for (i = 0; i < n2; i++) {
		x[i + n1] = x[i + n1] + x[i + n2 + n1];
	}
	x[n1 + n2] = x[n2 + n1] + x[n1 + n2 - 1] - x[n - 1];
	for (j = 0, i = 0; i < (n - 1); i++) {
		if (i < j) {
			t = x[j];
			x[j] = x[i];
			x[i] = t;
		}
		k = n / 2;
		while (k < (j + 1)) {
			j = j - k;
			k = k / 2;
		}
		j = j + k;
	}
	x[0] *= 1.0 / sqrt((double)n);
	t = sqrt(2.0 / n);
	for (i = 1; i < n; i++) {
		x[i] *= t;
	}

}