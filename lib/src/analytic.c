/**
 * 产生解析信号
 */
#include <math.h>
#include "../inc/signal_process.h"
/**
 * x ：		长度为n的double数组，存放解析信号的实部
 * y ：		长度为n的double数组，存放解析信号的实部
 * n ：		数据的长度,必须是2的整数次幂
 */
void analytic(double *x, double *y, int n)
{
	int i, n1;

	n1 = n / 2;
	for (i = 0; i < n; i++)
	{
		y[i] = 0.0;
	}
	fft(x, y, n, 1);
	for (i = 1; i < n1; i++)
	{
		x[i] = 2 * x[i];
		y[i] = 2 * y[i];
	}

	for (i = n1; i < n; i++)
	{
		x[i] = 0.0;
		y[i] = 0.0;
	}
	fft(x, y, n, -1);
}