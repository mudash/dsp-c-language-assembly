/**
 * 产生二项式分布的随机数
 */
#include <math.h>
#include "../inc/signal_generator.h"
/**
 * n :		二项式分布参数n
 * p :		二项式分布的参数p
 * s ：		*s为随机数种子
 * return ：	产生的随机数
 */
int bin(int n, double p, long int *s) 
{
	int i;
	int x;

	for (x = 0.0, i = 0; i < n; i++)
	{
		x += bn(p, s);
	}

	return (x);
}