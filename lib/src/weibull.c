/**
 * 产生韦伯分布的随机数
 */
#include <math.h>
#include "../inc/signal_generator.h"
/**
 * a :		韦伯分布的参数alpha
 * b ：		柯西分布的参数bate
 * s ：		*s为随机数种子
 * return ：	产生的随机数
 */
double weibull(double a, double b, long int *s) 
{
	double u, x;

	u = uniform(0.0, 1.0, s);
	u = -log(u);
	x = b * pow(u, 1.0 / a);

	return (x);
}