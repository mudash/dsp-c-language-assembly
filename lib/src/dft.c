/**
 * 计算复序列的离散傅里叶变换(DFT)和离散傅立叶反变换（IDFT）
 */
#include <math.h>
#include "../inc/signal_process.h"
/**
 *
 * x ：		长度为n的double数组,存放要变换的数据的实部
 * y ：		长度为n的double数组,存放要变换的数据的实部
 * a ：		长度为n的double数组,存放要变换的数据的实部
 * b ：		长度为n的double数组,存放要变换的数据的实部
 * n ：		数据长度n
 * sign ：	sign = 1计算DFT，sign = -1计算IDFT
 */
void dft(double *x, double *y, double *a, double *b, int n, int sign)
{
	int i, k;
	double c, d, q, w, s;
	q = 6.28318530718 / n;
	for (k = 0; k < n; k++)
	{
		w = k * q;
		a[k] = 0.0;
		b[k] = 0.0;
		for (i = 0; i < n ; i++)
		{
			d = i * w;
			c = cos(d);
			s = sin(d) * sign;
			a[k] += c * x[i] + s * y[i];
			b[k] += c * y[i] - s * x[i];
		}
	}
	if (sign == -1)
	{
		c = 1.0 / n;
		for (k = 0; k < n; k++)
		{
			a[k] = c * a[k];
			b[k] = c * b[k];
		}
	}
}