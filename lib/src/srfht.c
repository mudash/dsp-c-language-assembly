/**
 * 分裂基快速哈特莱(Hart1ey)变换算法
 */
#include <math.h>
#include <stdlib.h>
#include "../inc/signal_process.h"
/**
 * x ：		长度为n的double数组，[in]存放要变换数据的实数据，最后保存变换结果,
 * n ：		int数据, 输入数据长度,必须是2的m次方， m为正数
 */
void srfht(int n, double *x)
{
	int i, j, k, m, n1, n2, n4, n8, is, id;
	int l11, l12, l13, l14, l21, l22, l23, l24;
	double a, a3, e, c1, c3, s1, s3, st, t1, t2, t3, t4, t5;

	for (j = 1, i = 1; i < 16; i++) {
		m = i;
		j = 2 * j;

		if (j == n) {
			break;
		}
	}
	st = sqrt(2.0);
	n2 = 2 * n;
	for (k = 1; k < m; k++) {
		is = 0;
		id = n2;
		n2 = n2 / 2;
		n4 = n2 / 4;
		n8 = n2 / 8;
		e = 6.283185307179586 / n2;
		do {
			for (i = is; i < n; i += id) {
				l12 = i + n4;
				l13 = l12 + n4;
				l14 = l13 + n4;
				t1 = x[l12] - x[l14];
				t2 = x[i] + x[l13];
				t3 = x[l12] + x[l14];
				t4 = x[i] - x[l13];
				x[l14] = t4 - t1;
				x[l13] = t4 + t1;
				x[l12] = t3;
				x[i] = t2;
				if (n4 == 1) {
					continue;
				}
				l21 = i + n8;
				l22 = l21 + n4;
				l23 = l22 + n4;
				l24 = l23 + n4;
				t1 = x[l22];
				t2 = x[l23];
				t3 = x[l24];
				x[l24] = (t1 - t3) * st;
				x[l23] = (x[l21] - t2) * st;
				x[l22] = t1 + t3;
				x[l21] = x[l21] + t2;
				a = e;
				for (j = 1; j < n8; j++) {
					l11 = i + j;
					l12 = l11 + n4;
					l13 = l12 + n4;
					l14 = l13 + n4;
					l21 = i + n4 - j;
					l22 = l21 + n4;
					l23 = l22 + n4;
					l24 = l23 + n4;
					a3 = 3 * a;
					c1 = cos(a);
					s1 = sin(a);
					c3 = cos(a3);
					s3 = sin(a3);
					a = (j + 1) * e;
					t5 = x[l21] - x[l23];
					t2 = x[l11] - x[l13];
					t1 = t2 + t5;
					t2 = t2 - t5;
					t5 = x[l22] - x[l24];
					t4 = x[l14] - x[l12];
					t3 = t4 + t5;
					t4 = t4 - t5;
					x[l11] = x[l11] + x[l13];
					x[l12] = x[l12] + x[l14];
					x[l21] = x[l21] + x[l23];
					x[l22] = x[l22] + x[l24];
					x[l13] = t1 * c1 + t3 * s1;
					x[l14] = t2 * c3 - t4 * s3;
					x[l23] = t1 * s1 - t3 * c1;
					x[l24] = t2 * s3 + t4 * c3;
				}
			}
			is = 2 * id - n2;
			id = 4 * id;
		} while (is < (n - 2));
	}
	is = 0;
	id = 4;
	do {
		for (i = is; i < n; i += id) {
			t1 = x[i];
			x[i] = t1 + x[i + 1];
			x[i + 1] = t1 - x[i + 1];
		}
		is = 2 * id - 2;
		id = 4 * id;
	} while (is < (n - 1));
	n1 = n - 1;
	for (j = 0, i = 0; i < n1; i++) {
		if (i < j) {
			t1 = x[j];
			x[j] = x[i];
			x[i] = t1;
		}
		k = n / 2;
		while (k < (j + 1)) {
			j = j - k;
			k = k / 2;
		}
		j = j + k;
	}
}
