/**
 * 快速离散正弦变换
 */
#include <stdlib.h>
#include <math.h>
#include "../inc/signal_process.h"
/**
 * x ：		长度为n的double数组，开始存放变换输入数据,
 *			最后存放变换完成数据
 * n ：		int数据长度必须是2的整数次幂
 */
void fst(double *x, int n)
{
	int i, n1;
	double q, s, x1, *y;
	y = malloc(n * sizeof(double));
	n1 = n / 2;
	q = 4.0 * atan(1.0) / n;
	y[0] = 0.0;
	for (i = 1; i < n; i++) {
		y[i] = sin(i * q) * (x[i] + x[n - i]) + 0.5 * (x[i] - x[n - i]);
	}
	rfft(y, n);
	x1 = 0.0;
	for (i = 1; i < n; i++) {
		s = i * q;
		s = sin(s);
		x1 += s * x[i];
	}
	x[0] = 0.0;
	x[1] = x1;
	for (i = 1; i < n1; i++) {
		x[2 * i] = -y[n - i];
		x[2 * i + 1] = x[2 * i - 1] + y[i];
	}
	s = sqrt(2.0 / n);
	for (i = 1; i < n; i++) {
		x[i] *= s;
	}

	free(y);
}