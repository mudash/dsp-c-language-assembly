/**
 * 实序列快速傅里叶变换II
 */
#include <stdlib.h>
#include <math.h>
#include "../inc/signal_process.h"
/**
 * x ：		长度为n的double数组，[in]存放要变换数据的实部,
 * [out]存放变换完成数据的实部
 * n ：		int数据长度必须是2的整数次幂
 */
void rlfft(double *x, int n)
{
	int i, n1;
	double a, c, e, s, fr, fi, gr, gi, *f, *g;
	f = malloc(n / 2 * sizeof(double));
	g = malloc(n / 2 * sizeof(double));
	n1 = n / 2;
	e = 3.141592653589793 / n1;
	for (i = 0; i < n1; i++)
	{
		f[i] = x[2 * i];
		g[i] = x[2 * i + 1];
	}
	fft(f, g, n1, 1);
	x[0] = f[0] + g[0];
	x[n1] = f[0] - g[0];
	for (i = 1; i < n1; i++)
	{
		fr = (f[i] + f[n1 - i]) / 2;
		fi = (g[i] - g[n1 - i]) / 2;
		gr = (g[n1 - i] + g[i]) / 2;
		gi = (f[n1 - i] - f[i]) / 2;
		a = i * e;
		c = cos(a);
		s = sin(a);
		x[i] = fr + c * gr + s * gi;
		x[n - i] = fi - c * gi - s * gr;
	}
	free(f);
	free(g);
}