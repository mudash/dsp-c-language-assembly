/**
 * 快速离散正弦变换
 */
#include <stdlib.h>
#include <math.h>
#include "../inc/signal_process.h"
/**
 * x ：		长度为n的double数组，开始存放变换输入数据,
 *			最后存放变换完成数据
 * n ：		int数据长度必须是2的整数次幂
 * sign :	整型变量。变换类型的控制参数。
 *			当sign=1时，计算哈达玛排序的快速沃尔什变换;
 *			当sign=2时，计算沃尔什排序的快速沃尔什变换;
 *			当sign-3时，计算佩利排序的快速沃尔什变换。
 */
void fwt(double *x, int n, int sign)
{
	int i, j, j1, k, k1, l, m, n0, n1, n2, flag;
	double t;

	for (j = 1, i = 1; i < 16; i++) {
		m = i;
		j = 2 * j;
		if (j == n) {
			break;
		}
	}
	if (sign >= 2) {
		n1 = n - 1;
		for (j = 0, i = 0; i < n1; i++) {
			if (i < j) {
				t = x[j];
				x[j] = x[i];
				x[i] = t;
			}
			k = n / 2;
			while (k < (j + 1)) {
				j = j - k;
				k = k / 2;
			}
			j = j + k;
		}
	}
	n2 = 1;
	for (l = 1; l <= m; l++) {
		n0 = n / n2;
		n1 = n0 / 2;
		flag = 1;
		for (j = 0; j < n2; j++) {
			j1 = j * n0;
			for (i = 0; i < n1; i++) {
				k = i + j1;
				k1 = k + n1;
				t = x[k1] * flag;
				x[k1] = x[k] - t;
				x[k] = x[k] + t;
			}
			if (sign == 2) {
				flag = -flag;
			}
		}
		n2 = n2 * 2;
	}
	for (i = 0; i<n; i++) {
		x[i] /= n;
	}
}