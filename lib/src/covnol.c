#include <math.h>
#include "../inc/signal_process.h"

/**
 * x--双精度实型一维数组, 长度为len。开始时存放实序列x(:), 最后存放线性卷积的值。
 * y--双精度实型一维数组, 长度为n。存放实序列y(i)。
 * m--整型变量。序列x(i)的长度。
 * n--整型变量。序列y(i)的长度。
 * len -- 整型变量, 线性卷积的长度。len≥m + n - 1, 且必须是是2的整数次幂, 即len = 2'
 */
void convol(double x[], double y[], int m, int n, int len)
{
	int i, len2;
	double t;
	for (i = m; i < len; i++) {
		x[i] = 0.0;
	}
	for (i = n; i < len; i++) {
		y[i] = 0.0;
	}
	rfft(x, len);
	rfft(y, len);
	len2 = len / 2;
	x[0] = x[0] * y[0];
	x[len2] = x[len2] * y[len2];
	for (i = 1; i < len2; i++) {
		t = x[i] * y[i] - x[len - i] * y[len - i];
		x[len - i] = x[i] * y[len - i] + x[len - i] * y[i];
		x[i] = t;
	}
	irfft(x, len);
}