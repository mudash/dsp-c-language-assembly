/**
 * 快速离散正弦变换
 */
#include <stdlib.h>
#include <math.h>
#include "../inc/signal_process.h"
/**
 * x ：		长度为n的double数组，开始存放变换输入数据,
 *			最后存放变换完成数据
 * n ：		int数据长度必须是2的整数次幂
 */
void hilbtf(double *x, int n)
{
	int i, n1;
	double t;

	n1 = n / 2;
	rfft(x, n);
	for (i = 1; i < n1; i++) {
		t = x[i];
		x[i] = x[n - i];
		x[n - i] = -t;
	}

	x[0] = 0.0;
	x[n1] = 0.0;
	irfft(x, n);
}