
/**
 * 产生正太分布的随机数
 */
#include "../inc/signal_generator.h"
/**
 * mean :		正太分布的均值
 * sigma ：		正太分布的方差
 * seed ：		*seed为随机数种子
 * return ：	产生的随机数
 */
double gauss(double mean, double sigma, long int *seed) 
{
	int i;
	double x, y;

	for (x = 0.0, i = 0; i < 12; i++)
	{
		x += uniform(0.0, 1.0, seed);
	}
	x = x - 6.0;
	y = mean + x * sigma;

	return (y);
}