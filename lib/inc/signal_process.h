#ifndef __SIGNAL_PROCESS_H__
#define __SIGNAL_PROCESS_H__

#ifdef __cplusplus
extern "C" {
#endif

extern void dft(double *x, double *y, double *a, double *b, int n, int sign);

extern void fft(double *x, double *y, int n, int sign);

extern void fft4(double *x, double *y, int n);

extern void srfft(double *x, double *y, int n);

extern void rfft(double *x, int n);

extern void rlfft(double *x, int n);

extern void irfft(double *x, int n);

extern void pft(double *x, double *y, int n, int m, int *ni);

extern void czt(double *xr, double *xi, int n, int m, double f1, double f2);

extern void fht(int n, double *x);

extern void fht4(int n, double *x);

extern void srfht(int n, double *x);

extern void fct(double *x, int n);

extern void ifct(double *x, int n);

extern void fct8(double *x, int n);

extern void ifct8(double *x, int n);

extern void fst(double *x, int n);

extern void fwt(double *x, int n, int sign);

extern void hilbth(double *x, int n);

extern void hilbtf(double *x, int n);

extern void convol(double x[], double y[], int m, int n, int len);

#ifdef __cplusplus
}
#endif

#endif