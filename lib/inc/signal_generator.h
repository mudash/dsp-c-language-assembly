#ifndef __SIGNAL_GENERATOE_H__
#define __SIGNAL_GENERATOE_H__

#ifdef __cplusplus
extern "C" {
#endif

extern double uniform(double a, double b, long int *seed);

extern double gauss(double mean, double sigma, long int *seed);

extern double exponent(double beta, long int *s);

extern double laplace(double beta, long int *s);

extern double rayleigh(double sigma, long int *s);

extern double lognorm(double u, double sigma, long int *s);

extern double cauchy(double a, double b, long int *s);

extern double weibull(double a, double b, long int *s);

extern int bn(double p, long int *s);

extern double bg(double p, double mean, double sigma, long int *s);

extern int bin(int n, double p, long int *s);

extern int poisson(double lambda, long int *s);

extern void arma(double *a, double *b, int p, int q, double mean, double sigma, long int *seed, double *x, int n);

extern void sinwn(double *a, double *f, double *ph, int m, double fs, double snr, long seed, double *x, int n);

extern void analytic(double *x, double *y, int n);

#ifdef __cplusplus
}
#endif

#endif