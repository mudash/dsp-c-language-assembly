
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
 */

int main()
{
	int i, n;
	double x[64], y[64];
	n = 64;

	for (i = 0; i < n; i++) {
		x[i] = sin(2 * 3.14159265 * i / n);
	}
	for (i = 0; i < n; i++) {
		y[i] = -cos(2 * 3.14159265 * i / n);
	}
	printf("\n Ideal Discrete Hilbert Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", y[i], y[i + 1]);
		printf(" %10.7f %10.7f", y[i + 2], y[i + 3]);
		printf("\n");
	}
	hilbtf(x, n);

	if (n != 64) {
		printf("\n n!= 64 set n is 64\n");
		n = 64;
	}

	printf("\n Discrete Hilbert Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	getchar();

	return 0;
}