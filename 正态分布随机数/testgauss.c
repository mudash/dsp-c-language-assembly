/**
 * 产生50个均值为0，方差为1的正太分布的随机数
 */
#include <stdio.h>
#include "../lib/inc/signal_generator.h"

int main()
{
	int i,j;
	long int s;
	double x, mean, sigma;
	
	mean = 0.0;
	sigma = 1.0;
	s = 13579;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 5; j++)
		{
			x = gauss(mean, sigma, &s);
			printf("%13.7f", x); 
		}
		printf("\n");
	}
	getchar();
}