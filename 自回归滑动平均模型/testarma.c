/**
 * ARMA(2,2)模型为：
 * x(k)+1.45x(k-1)+0.6x(k-2)=w(k)-0.2w(k)-0.1w(k)
 * 其中w(k)是均值为0，方差为0.5的高斯白噪声，产生200个数据并将其存于数
 * 文件arma.dat中
 */
#include <stdio.h>
#include "../lib/inc/signal_generator.h"

int main()
{
	int i, n, p, q;
	long seed;
	double  mean, sigma, x[200];
	static double a[3] = {1.0, 1.45, 0.6};
	static double b[3] = { 1.0, -0.2, -0.1 };
	FILE *fp;
	n = 200;
	p = 2;
	q = 2;
	seed = 13579l;
	mean = 0.0;
	sigma = 0.5;
	arma(a,b,p,q,mean,sigma,&seed,x,n);
	for (i = 0; i < 32; i += 4)
	{

		printf("	%10.7lf		%10.7lf", x[i], x[i + 1]); 
		printf("	%10.7lf		%10.7lf", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fp = fopen("arma.dat", "w");
	for (i = 0; i < n; i++)
	{
		fprintf(fp, "%d %lf\n", i, x[i]);
	}
	fclose(fp);
	getchar();
}