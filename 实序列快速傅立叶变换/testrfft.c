
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
*/

int main()
{
	int i, j, n;
	double x[64];
	n = 64;
	for (i = 0; i < 10; i++)
	{
		x[i] = 0.0;
	}
	for (i = 10; i < n; i++)
	{
		x[i] = exp(-(i - 10) / 15.0) * sin(6.2831853 * (i - 10) / 16.0);
	}
	rfft(x, n);
	printf("\n DISCRETE FOURIER TRANSFORM \n");
	printf(" %10.7f               ", x[0]);
	printf(" %10.7f + j %10.7f\n", x[1], x[n - 1]);
	for (i = 2; i < n / 2; i += 2)
	{
		printf(" %10.7f + j %10.7f", x[i], x[n - i]);
		printf(" %10.7f + j %10.7f", x[i + 1], x[n - i - 1]);
		printf("\n");
	}
	printf(" %10.7f               ", x[n / 2]);
	printf(" %10.7f + j %10.7f\n", x[n / 2 - 1], -x[n / 2 + 1]);
	for (i = 2; i < n / 2; i += 2)
	{
		printf(" %10.7f + j %10.7f", x[n / 2 - i], -x[n / 2 + i]);
		printf(" %10.7f + j %10.7f", x[n / 2 - i - 1], -x[n / 2 + i + 1]);
		printf("\n");
	}

	getchar();
}