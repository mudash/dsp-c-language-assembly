/**
 * 产生50个参数lambda = 0.4 的泊松布的随机数
 */
#include <stdio.h>
#include "../lib/inc/signal_generator.h"

int main()
{
	int i, j, x;
	long int s;
	double  n;
	
	n = 4.0;
	s = 13579;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 5; j++)
		{
			x = poisson(n, &s);
			printf("%11d", x); 
		}
		printf("\n");
	}
	getchar();
}