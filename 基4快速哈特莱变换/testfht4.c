
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
 * 设输入序列x(i)为
 * x(i)= e^-0.5i, i=0,1,...,n-1; 
 * 选取序列长度 n=16用基 4 快速算法计算 x(i)的离散哈特莱变换
 */

int main()
{
	int i, n;
	double x[16];
	n = 16;
	for (i = 0; i < n; i++) {
		x[i] = exp(-0.5 * i);
	}
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fht4(n, x);
	printf("\n DISCRETE HARTLEY TRANSFORM\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	getchar();

	return 0;
}