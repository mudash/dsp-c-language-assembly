
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
 */

int main()
{
	int i, j, m, n, len, len1;
	double z[16];
	double x[16] = { -1,5,2,7,6,9,4 }, y[16] = { 1,2,1,-3,4,5 };
	m = 7;
	n = 6;
	len = 16;
	len1 = m + n - 1;
	for (j = 0; j < len1; j++) {
		z[j] = 0.0;
		for (i = 0; i < m; i++) {
			if (((j - i) >= 0) && ((j - i) < n)) {
				z[j] += x[i] * y[j - i];
			}
		}
	}
	printf("Direct Caiculation of Linear Convolution\n");
	for (i = 0; i < len1; i++) {
		printf("		%10.1lf", z[i]);
		if (i % 4 == 3) {
			printf("\n");
		}
	}
	printf("\n");
	convol(x, y, m, n, len);
	printf("Fast Calculation of Linear Convolution\n\n");
	for (i = 0; i < len1; i++) {
		printf("		%10.1lf", x[i]);
		if (i % 4 == 3) {
			printf("\n");
		}
	}
	printf("\n");
	getchar();

	return 0;
}