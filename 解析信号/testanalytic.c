
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_generator.h"

/**
* 测试1：
* 单正弦信号，振幅为1，频率为5Hz，相位为45度，无噪声干扰。
* 产生200个数据保存于文件sinwn1.dat中
*/

int main()
{
	int i, n;
	double x[64], y[64], z[64];
	n = 64;
	for (i = 0; i < n; i++)
	{
		x[i] = sin(2 * 3.14159265 * i / n);
	}
	printf("\n The Original Signals \n");
	for (i = 0; i < n / 2; i += 4)
	{
		printf("	%10.7lf	%10.7lf", x[i], x[i + 1]);
		printf("	%10.7lf	%10.7lf\n", x[i + 2], x[i + 3]);
	}

	for (i = 0; i < n; i++)
	{
		z[i] = -cos(2 * 3.14159265 * i / n);
	}
	printf("\n Ideal Discrete Hilbert Transform \n");
	for (i = 0; i < n / 2; i += 4)
	{
		printf("	%10.7lf	%10.7lf", z[i], z[i + 1]);
		printf("	%10.7lf	%10.7lf\n", z[i + 2], z[i + 3]);
	}
	analytic(x, y, n);
	printf("\n Real Part of Analytic Signal \n");
	for (i = 0; i < n / 2; i += 4)
	{
		printf("	%10.7lf	%10.7lf", x[i], x[i + 1]);
		printf("	%10.7lf	%10.7lf\n", x[i + 2], x[i + 3]);
	}
	printf("\n Imaginary Part of Analytic Signal \n");
	for (i = 0; i < n / 2; i += 4)
	{
		printf("	%10.7lf	%10.7lf", y[i], y[i + 1]);
		printf("	%10.7lf	%10.7lf\n", y[i + 2], y[i + 3]);
	}
	getchar();
}