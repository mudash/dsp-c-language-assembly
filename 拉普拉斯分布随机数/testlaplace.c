/**
 * 产生50个参数为1.50的拉普拉斯分布的随机数
 */
#include <stdio.h>
#include "../lib/inc/signal_generator.h"

int main(void)
{
	int i,j;
	long int s;
	double x, beta;
	beta = 1.5;
	s = 13579;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 5; j++)
		{
			x = laplace(beta, &s);
			printf(" %13.7f", x);
		}
		printf("\n");
	}
	getchar();
}