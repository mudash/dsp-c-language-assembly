
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
*/

int main()
{
	int i, n;
	double x[64];
	FILE *fp;
	n = 64;
	for (i = 0; i < 10; i++)
	{
		x[i] = 0.0;
	}
	for (i = 10; i < n; i++)
	{
		x[i] = exp(-(i - 10) / 15.0) * sin(6.2831853 * (i - 10) / 16.0);
	}
	rlfft(x, n);
	printf("\n DISCRETE FOURIER TRANSFORM \n");
	printf(" %10.7f               ", x[0]);
	printf(" %10.7f + j %10.7f\n", x[1], x[n - 1]);
	for (i = 2; i < n / 2; i += 2)
	{
		printf(" %10.7f + j %10.7f", x[i], x[n - i]);
		printf(" %10.7f + j %10.7f", x[i + 1], x[n - i - 1]);
		printf("\n");
	}
	printf(" %10.7f               \n", x[n / 2]);
	for (i = 1; i < n / 2; i++)
	{
		x[i] = sqrt(x[i] * x[i] + x[n - i] * x[n - i]);
	}
	x[n / 2] = fabs(x[n / 2]);
	fp = fopen("rlfft.dat", "w");
	for (i = 0; i <= n / 2; i++)
	{
		fprintf(fp, "%d, %f\n", i, x[i]);
	}

	getchar();
}