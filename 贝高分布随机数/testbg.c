/**
 * 产生50个参数p = 0.4, mean = 0, sigma = 1 的贝努里-高斯分布的随机数
 */
#include <stdio.h>
#include "../lib/inc/signal_generator.h"

int main()
{
	int i,j;
	long int s;
	double x, p, mean, sigma;
	
	p = 0.4;
	mean = 0.0;
	sigma = 1.0;
	s = 12357;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 5; j++)
		{
			x = bg(p, mean, sigma, &s);
			printf("%13.7f", x); 
		}
		printf("\n");
	}
	getchar();
}