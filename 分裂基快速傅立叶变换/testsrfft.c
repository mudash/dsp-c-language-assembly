
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
*/

int main()
{
	int i, j, n;
	double a1, a2, x[32], y[32];
	n = 32;
	a1 = 0.9;
	a2 = 0.3;
	x[0] = 1.0;
	y[0] = 0.0;
	for (i = 1; i < n; i++)
	{
		x[i] = a1 * x[i - 1] - a2 * y[i - 1];
		y[i] = a2 * x[i - 1] + a1 * y[i - 1];
	}
	srfft(x, y, n);
	printf("\n DISCRETE FOURIER TRANSFORM \n");
	for (i = 0; i < n / 2; i++)
	{
		for (j = 0; j < 2; j++)
			printf(" %10.7f + J %10.7f", x[2 * i + j], y[2 * i + j]);
		printf("\n");
	}
	getchar();
}