
#include <stdio.h>
#include "../lib/inc/signal_generator.h"

/**
* 测试1：
* 单正弦信号，振幅为1，频率为5Hz，相位为45度，无噪声干扰。
* 产生200个数据保存于文件sinwn1.dat中
*/
void sinwntest1(void)
{
	int i, m, n;
	long seed;
	double  fs, snr, x[200];
	static double a[1] = { 1.0};
	static double f[1] = { 5.0};
	static double ph[1] = { 45 };
	FILE *fp;
	n = 200;
	m = 1;
	seed = 13579;
	fs = 150;
	snr = 1000.0;
	sinwn(a, f, ph, m, fs, snr, seed, x, n);
	printf("\n Single Sinusoidal Signal \n");
	for (i = 0; i < 32; i++)
	{
		printf("	%10.7f", x[i]);
		if (i % 4 == 3)
		{
			printf("\n");
		}
	}
	fp = fopen("sinwn1.dat", "w");
	for (i = 0; i < n; i++)
	{
		fprintf(fp, "%3d	%12.7f\n", i, x[i]);
	}
	fclose(fp);
}

/**
 * 测试2：
 * 三个正玄信号，振幅为A1 = A2 = A2 = 1，频率为 f1 = 10 f2 = 17 
 * f3 = 50，相位为ph1 = 45度 ph2 = 10度 ph3 = 88度，
 * 采样频率fs = 150Hz 该正弦组合信号受到高斯白噪声N（0，mean^2）
 * 干扰,信噪比SNR = 5dB。
 * 产生200个数据保存于文件sinwn2.dat中
 */
void sinwntest2(void)
{
	int i, m, n;
	long seed;
	double  fs, snr, x[200];
	static double a[3] = { 1.0, 1.0, 1.0};
	static double f[3] = { 10.0, 17.0, 50.0 };
	static double ph[3] = { 45, 10, 88 };
	FILE *fp;
	n = 200;
	m = 3;
	seed = 13579l;
	fs = 150;
	snr = 5.0;
	sinwn(a, f, ph, m, fs, snr, seed, x, n);
	printf("\n Three Sinusoidal Signals plus Gauss White Noise \n");
	for (i = 0; i < 32; i++)
	{
		printf("	%10.7lf", x[i]);
		if (i % 4 == 3)
		{
			printf("\n");
		}
	}
	fp = fopen("sinwn2.dat", "w");
	for (i = 0; i < n; i++)
	{
		fprintf(fp, "%3d	%12.7lf\n", i, x[i]);
	}
	fclose(fp);
}
int main()
{
	sinwntest1();
	sinwntest2();
	getchar();
}