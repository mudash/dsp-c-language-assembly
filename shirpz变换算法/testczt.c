
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
*/

int main()
{
	int i, n, m;
	double f1, f2, x;
	double xr[512], xi[512];
	FILE *fp;

	n = 64;

	for (i = 0; i < 10; i++) {
		xr[i] = 0.0;
		xi[i] = 0.0;
	}

	for (i = 10; i < n; i++)
	{
		xr[i] = exp(-(i - 10) / 15.0) * sin(6.2831853 * (i - 10) / 16.0);
		xi[i] = 0.0;
	}

	f1 = 0.0;
	f2 = 0.25;
	m = 200;
	czt(xr, xi, n, m, f1, f2);
	printf("\nCHIRP Z-TRANSFORM\n");
	for (i = 0; i < m; i += 2)
	{
		printf("%10.7f + J\t%10.7f\t", xr[i], xi[i]);
		printf("%10.7f + J\t%10.7f", xr[i + 1], xi[i + 1]);

		printf("\n");
	}
	for (i = 0; i < m; i++)
	{
		xr[i] = sqrt(xr[i] * xr[i] + xi[i] * xi[i]);
	}
	fp = fopen("czt.dat", "w");
	for (i = 0; i < m; i++)
	{
		x = f1 + i * (f2 - f1) / (m - 1);
		fprintf(fp, "%f %f\n", x, xr[i]);
	}
	fclose(fp);
	getchar();

	return 0;
}