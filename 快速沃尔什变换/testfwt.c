
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
*/

int main()
{
	int i, n;
	static double x[8] = { 1.0, 2.0, 1.0, 1.0, 3.0, 2.0, 1.0, 2.0 };
	n = 8;
	fwt(x, n, 1);
	printf("\n Hadamard-ordered Walsh Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fwt(x, n, 1);
	for (i = 0; i < n; i++) {
		x[i] = n * x[i];
	}
	printf("\n Inverse Hadamard-ordered Walsh Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fwt(x, n, 2);
	printf("\n Walsh-ordered Walsh Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fwt(x, n, 2);
	for (i = 0; i < n; i++) {
		x[i] = n * x[i];
	}
	printf("\n Inverse Walsh-ordered Walsh Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fwt(x, n, 3);
	printf("\n Paley-ordered Walsh Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fwt(x, n, 3);
	for (i = 0; i < n; i++) {
		x[i] = n *x[i];
	}
	printf("\n Inverse Paley-ordered Walsh Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	getchar();

	return 0;
}