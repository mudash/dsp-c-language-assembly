//产生50个参数a = 1，b = 1 的柯西分布的随机数
#include <stdio.h>
#include "../lib/inc/signal_generator.h"

int main(void)
{
	int i,j;
	long int s;
	double x, alpha, beta;
	alpha = 1.0;
	beta = 1.0;
	s = 13579;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 5; j++)
		{
			x = cauchy(alpha, beta, &s);
			printf(" %13.7f", x);
		}
		printf("\n");
	}
	getchar();
}