
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
*/

int main()
{
	int i, n;
	double x[32];
	n = 32;
	for (i = 0; i < 10; i++) {
		x[i] = 0.0;
	}
	for (i = 10; i < n; i++) {
		x[i] = exp(-(i - 10) / 15.0) * sin(6.2831853 * (i - 10) / 16.0);
	}
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fht(n, x);
	printf("\n DISCRETE HARTLEY TRANSFORM\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	getchar();

	return 0;
}