
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
*/

int main()
{
	int i, j, n, m, ni[5];
	double al, a2, x[45], y[45];
	n = 45;
	m = 2; 
	ni[0] = 5;
	ni[1] = 9;
	al = 0.9;
	a2 = 0.3;
	x[0] = 1.0;
	y[0] = 0.0;
	for (i = 1; i < n; i++)
	{
		x[i] = al * x[i - 1] - a2 * y[i - 1];
		y[i] = a2 * x[i - 1] + al * y[i - 1];
	}
	pft(x, y, n, m, ni);
	printf("\n DISCRETE FOURIER TRANSFORM\n");
	for (i = 0; i < n / 2; i++)
	{
		for (j = 0; j < 2; j++)
		{
			printf("%10.7f + J%10.7f", x[2 * i + j], y[2 * i + j]);
		}
		printf("\n");
	}
	printf("%11.7f+J%117f\n", x[n - 1], y[n - 1]);

	getchar();
}