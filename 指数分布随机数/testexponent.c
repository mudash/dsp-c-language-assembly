//产生50个均值为2，方差为4的指数分布的随机数
#include <stdio.h>
#include "../lib/inc/signal_generator.h"
int main()
{
	int i,j;
	long int s;
	double x, beta;
	beta = 2.0;
	s = 13579;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 5; j++)
		{
			x = exponent(beta, &s);
			printf(" %13.7f", x);
		}
		printf("\n");
	}
	getchar();
}