
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
 */

int main()
{
	int i, n;
	double x[64];
	n = 64;
	FILE *fp;

	for (i = 0; i < n; i++) {
		x[i] = sin(2 * 3.14159265 * i / 32);
	}
	fp = fopen("hilbl.dat", "w");
	for (i = 0; i < n; i++) {
		fprintf(fp, "%d\t %f\n", i, x[i]);
	}
	fclose(fp);
	printf("\n Original Sequence\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	hilbth(x, n);
	printf("\n Discrete Hilbert Transform\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fp = fopen("hilb2dat", "w");
	for (i = 0; i < n; i++) {
		fprintf(fp, "%d\t %f\n", i, x[i]);
	}
	fclose(fp);
	getchar();

	return 0;
}