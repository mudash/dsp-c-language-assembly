/**
 * 产生50个参数p = 0.7 的贝努里布的随机数
 */
#include <stdio.h>
#include "../lib/inc/signal_generator.h"

int main()
{
	int i,j;
	long int s;
	double x, p;
	
	p = 0.7;
	s = 13579;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 5; j++)
		{
			x = bn(p, &s);
			printf("%13.7f", x); 
		}
		printf("\n");
	}
	getchar();
}