
#include <stdio.h>
#include <math.h>
#include "../lib/inc/signal_process.h"

/**
*/

int main()
{
	int i, n;
	double x[16];
	n = 16;
	for (i = 0; i < n; i++) {
		x[i] = exp(-0.5 * i);
	}
	printf("INPUT SEQUENCE\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fst(x, n);
	printf("\n DISCRETE SINE TRANSFORM\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}
	fst(x, n);
	printf("\n INVERSEI DISCRETE SINE TRANSFORM\n");
	for (i = 0; i < n; i += 4) {
		printf(" %10.7f %10.7f", x[i], x[i + 1]);
		printf(" %10.7f %10.7f", x[i + 2], x[i + 3]);
		printf("\n");
	}

	getchar();

	return 0;
}